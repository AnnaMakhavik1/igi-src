package by.gsu.igi.students.MakhavikAnna.lab3;

import java.util.Scanner;

/**
 * Created by Анна Маховик on 20.10.2017.
 */
public class SpeedConverter {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter speed (kmh,mph,kn,ms):");
        float speed = in.nextFloat();
        String unit = in.nextLine().trim();
        System.out.println("Source data: " + speed + unit);
        //Translated data from kmh
        String kmh = "kmh";
        if (unit.equals(kmh)) {
            speed = (speed * 1000) / 3600;
            System.out.println(speed + " ms");
        }
        //Translated data from mph
        String mph = "mph";
        double mile = 0.44704;
        float fmile = (float) mile;
        if (unit.equals(mph)) {
            speed = speed * fmile;
            System.out.println(speed + " ms");
        }
        //Translated data from kn
        String kn = "kn";
        double knot = 0.5144;
        float fknot = (float) knot;
        if (unit.equals(kn)) {
            speed = speed * fknot;
            System.out.println(speed + " ms");
        }
        //Translated data from ms
        String ms = "ms";
        if (unit.equals(ms))
            System.out.println(speed + " ms");
        //Error handling
        if (!unit.equals(ms) & !unit.equals(kn) & !unit.equals(mph) & !unit.equals(kmh))
            System.out.println("The data entered is incorrect.");
    }
}
